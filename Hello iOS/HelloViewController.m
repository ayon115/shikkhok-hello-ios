//
//  HelloViewController.m
//  Hello iOS
//
//  Created by Ashiq uz Zoha on 5/8/14.
//  Copyright (c) 2014 Shikkhok. All rights reserved.
//

#import "HelloViewController.h"

@interface HelloViewController ()

@end

@implementation HelloViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onClickButton:(id)sender {
    
    NSLog(@"Hello World Button Clicked");
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hello iOS" message:@"This is my first iOS Application" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil , nil];
    [alert show];

}

@end
