//
//  main.m
//  Hello iOS
//
//  Created by Ashiq uz Zoha on 5/8/14.
//  Copyright (c) 2014 Shikkhok. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
