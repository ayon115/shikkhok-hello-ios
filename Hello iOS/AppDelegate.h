//
//  AppDelegate.h
//  Hello iOS
//
//  Created by Ashiq uz Zoha on 5/8/14.
//  Copyright (c) 2014 Shikkhok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
